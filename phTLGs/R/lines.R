
#' Time profiles of ALT, AST, ALP and BILI for patients meeting the hy's law criteria.
#'
#'
#' @param dat dataset that you are going to use. It has to be a data frame.
#' @param lbtest The column which contains the lab test name.
#' @param lab_test1 Name of Test 1
#' @param lab_test2 Name of Test 2
#' @param lab_test3 Name of Test 3
#' @param lab_test4 Name of Test 4
#' @param lbstresn Numeric result of lab test.
#' @param lbstnrhi Reference range upper limit
#' @param subject_id The column which contains subject IDs.
#' @param visit_number The column which contains visit number.
#' @param analysis_day The column name which contains day of analysis.
#' @param colourpalette Set of colours
#' @return Line graphs for patients meeting hy's law criteria.
#' @import ggplot2
#' @import dplyr
#' @author venturedive
#' @export
lineGraphs <- function (dat,lbtest,lab_test1,lab_test2,lab_test3,lab_test4,lbstresn,lbstnrhi,
                        subject_id,visit_number,analysis_day, colourpalette = c("red", "blue", "green", "yellow")
){
  # getting R2A1Hi
  dpo1<- mutate(dat, r2a1hi = dat[,lbstresn]/dat[,lbstnrhi])
  dpo1 <- subset(dpo1,dpo1[[lbtest]] %in% c(lab_test1,lab_test2,lab_test3,lab_test4))

  #HYs law step 1 R2A1HI >3 for AST, ALT
  dpo2 <- subset(dpo1,dpo1$r2a1hi>=3)
  dpo2 <- subset(dpo2,dpo2[[lbtest]] %in% c(lab_test2,lab_test4))
  listsub <- dpo2[subject_id]
  listsub <- unique(listsub)
  listsub <- as.list(listsub)

  dpo1 <- subset(dpo1, dpo1[[subject_id]] %in% listsub[[subject_id]])
  #print(nrow(dpo1))

  #dfa <- subset(adl1, (USUBJID == '01-701-1015' | USUBJID == '01-701-1028' | USUBJID == '01-703-1258') & (LBTESTCD == 'AST' | LBTESTCD == 'ALT' | LBTESTCD == 'ALP' | LBTESTCD == 'BILI') & ANLDY > 0 )
  p <- ggplot(data=dpo1, aes(x=dpo1[,analysis_day], y=r2a1hi, group = dpo1[,lbtest])) + geom_line(aes(color= dpo1[,lbtest])) + scale_color_manual(values=colourpalette) + geom_point(aes(shape=NA)) + theme_classic() + xlab("Study Day") + ylab("xULN")
  p <- p + geom_hline( yintercept=1, linetype="dashed", color="blue") + geom_hline(yintercept=3, linetype="dashed", color="red")
  p + facet_wrap( ~ USUBJID, ncol= 4) #+ geom_vline(xintercept=dpo1[['TRTDUR']])
}

