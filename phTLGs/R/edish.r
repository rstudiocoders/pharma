#' edish plot of BILI ULN vs ALT ULN.
#'
#' @param data dataset that you are going to use. It has to be a data frame.
#' @param inc_col represents the column name for Lab tests.
#' @param inc_val represents the particular test types, one needs an evaluation for.
#'     It can take vector as well. These values must be present in inc_col
#' @param exc_col represents the column name that has Visits.
#' @param exc_val represents the values from exc_col to be excluded.
#' @param inc_val3 represents the value/s you want to include from exc_col.
#' @param lb_val represents the value numeric value of tests,in each visit.
#' @param subjids represents unique user identification.
#' @param treatment the column name where treatment types are residing.
#' @param blhi the column name where Baseline ULN resides.
#' @param print2 helps in getting the graph printed to word document or pdf.
#'               Just assign value to "doc" for word document and "pdf" for pdf.
#'               "jpeg","png","tiff","svg" could be used to print graph in specific file type.
#'               If nothing entered then only graph is plotted
#' @param plot_width Optional but needs to be added if printed graph in word doc or pdf is to be of specific width.
#' @param plot_height Optional but needs to be added if printed graph in word doc or pdf is to be of specific height.
#' @param doc_template A path of local word document,if you want to use template for gaprh to be plotted
#' @param xlabel user defined x-axis label
#' @param ylabel user defined y-axis label
#' @param x_scale custom define scale among "log", "log10","log1p", "log2", "reciprocal","reverse","sqrt"
#' @param y_scale custom define scale among "log", "log10","log1p", "log2", "reciprocal","reverse","sqrt"
#' @param footnote value to be displayed in footnote.
#' @param destination_file_name the specific name of output file.
#' @return Graph
#' @import dplyr
#' @import ggplot2
#' @import ggrepel
#' @import officer
#' @import svglite
#' @import reshape2
#' @author VentureDive
#' @export

edish <-
  function(data,
           inc_col,
           inc_val,
           exc_col,
           exc_val,
           inc_val3,
           lb_val,
           subjids,
           treatment,
           blhi,
           print2 = c("doc", "pdf", "jpeg", "png", "tiff",  "svg"),
           plot_width,
           plot_height,
           doc_template,
           xlabel ,
           ylabel,
           x_scale = c("identity","log", "log10", "log1p", "log2", "reciprocal", "reverse", "sqrt")
           ,
           y_scale = c("identity","log", "log10", "log1p", "log2", "reciprocal", "reverse", "sqrt"),
           footnote,
           destination_file_name) {

    ########################################
    ### Checking missing values
    if (missing(data))
      stop("missing(data)Need to specify data for graph.")

    if (missing(inc_col))
      stop("missing(inc_col) Need to specify column name for lab tests.")

    if (missing(inc_val))
      stop("missing(inc_val) Lab tests are missing.")

    if (missing(exc_col))
      stop("missing(exc_col) Need to specify column name that has Visits.")

    if (missing(subjids))
      stop("missing(subjids) Need to specify column in subjids for subject ids.")

    if (missing(treatment))
      stop("missing(treatment) Need to specify column in treatment_col for treatments.")

    if (missing(lb_val))
      stop("missing(lb_val) Need to specify column where test values are residing")

    if (missing(inc_val3))
      stop("missing(inc_val3) Need to specify the value you want to in Visits (exc_col)")

    if (missing(blhi))
      stop("missing(blhi) Need to specify column name where Baseline ULN resides")

    #subseting
    if (missing(exc_val)) {
      kk <- subset(data, data[[inc_col]] %in% inc_val)

      tt <-
        subset(data, data[[inc_col]] %in% inc_val &
                 (data[[exc_col]] %in% inc_val3))
    }

    else {
      kk <<-
        subset(data, data[[inc_col]] %in% inc_val &
                 !(data[[exc_col]] %in% exc_val))

      tt <<-
        subset(data,
               data[[inc_col]] %in% inc_val &
                 (data[[exc_col]] %in% inc_val3) & !(data[[exc_col]] %in% exc_val))

    }
    #finding ULN
    #calculating number of labtests included by user
    pkd <- c()
    for (i in 1:length(inc_val)) {
      pkd[i] <-
        mean(data[which(data[, inc_col] == inc_val[i]), blhi], na.rm = T)
    }

    #Calculating x and y axis values
    f = as.formula(paste0(lb_val, "~", subjids, "+", inc_col, "+", treatment))
    adlbc33max <<- aggregate(f, kk, max)

    colnames(adlbc33max)[4] <-
      paste("MAX_", colnames(adlbc33max)[4], sep = "")

    adlbc44max <<- aggregate(f, tt, max)


    dfk3 <<-
      merge.data.frame(adlbc33max,
                       adlbc44max,
                       all.x = TRUE,
                       suffixes = c(".x", ".y"))


    #transformation function to keep axis with one single decimal value
    scaleFUN <- function(x)
      sprintf("%.1f", x)

    print(adlbc33max)

    casted <- dcast(adlbc33max, usubjid + trtp ~ paramcd)

    print(casted)
    #plotting
    sp <- ggplot() +
      geom_point(
        data = casted,
        aes_string(
          colnames(casted)[3],
          colnames(casted)[4]
        ),
        position = position_jitter(width = 1, height = .5)
      ) +
      scale_shape_discrete(solid = F) +
      facet_grid(as.formula(paste('. ~', names(casted)[2]))) +
      geom_vline(xintercept = 3,
                 linetype = "dotted") +
      geom_hline(yintercept = 2,
                 linetype = "dotted") +
      # to make each plot look like a square
      coord_fixed(ratio = max(dfk3[, 5] , na.rm = TRUE) / max(dfk3[, 4] , na.rm = TRUE)) +
      theme(legend.position = "bottom",
            legend.background = element_rect(fill = "grey95"),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black")
      ) +
      theme(
        panel.background = element_rect(fill = "grey95"),
        plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm"),
        plot.background = element_rect(
          fill = "grey95",
          colour = "grey95",
          size = 1
        ),
        plot.caption = element_text(size = 7)
      )

    # to handel scales
    if (missing(x_scale)) {
      sp
    } else {
      sp <- sp + scale_x_continuous(trans = x_scale, labels = scaleFUN)
    }
    if (missing(y_scale)) {
      sp
    } else {
      sp <- sp + scale_y_continuous(trans = y_scale, labels = scaleFUN)
    }

    #to handle missing footnote
    if (missing(footnote)) {
      sp
    } else {
      sp <- sp + labs(caption = footnote)
    }


    # to handel missing x and y user defined labels
    if (missing(xlabel)) {
      sp
    } else {
      sp <- sp + labs(x = xlabel)
    }
    if (missing(ylabel)) {
      sp
    } else {
      sp <- sp + labs(y = ylabel)
    }


    #in case of missing height and width seeting default width and height to plot window size
    #i.e. dependant on device
    if (missing(plot_width)) {
      plot_width = 12.14
    }
    if (missing(plot_height)) {
      plot_height = 6.27
    }

    if (missing(destination_file_name)) {
      destination_file_name = "plot_trellis"
    }

    rlist <- NULL
    rlist[['graph']] <- sp
    rlist[['df']] <- dfk3[which(dfk3[4] > pkd[1]
                                & dfk3[5] > pkd[1]), ]


    # getting as normal graph
    if (missing(print2)) {
      return(rlist)
    }

    else {
      #Storing to document
      if (print2 == "doc") {
        if (missing(doc_template)) {
          doc = doc <- read_docx()
          doc <-
            body_add_gg(
              doc,
              value = sp,
              style = "centered",
              height = plot_height,
              width = plot_width
            )
        }
        else {
          doc = read_docx(path = doc_template)
          doc <-
            body_add_gg(doc,
                        value = sp,
                        height = plot_height,
                        width = plot_width)
        }
        print(doc, target = paste0(destination_file_name, ".docx"))
      }

      #saving to pdf
      else if (print2 == "pdf") {
        ggsave(
          paste0(destination_file_name, ".pdf"),
          sp,
          width = plot_width,
          height = plot_height
        )
        return(print(paste(
          "Stored in .pdf at",
          getwd(),
          paste0(destination_file_name, ".pdf")
        )))

      }
      #saving to jpeg
      else if (print2 == "jpeg") {
        ggsave(
          paste0(destination_file_name, ".jpeg"),
          sp,
          width = plot_width,
          height = plot_height
        )
        return(print(paste(
          "Stored in .jpeg format at",
          getwd(),
          paste0(destination_file_name, ".jpeg")
        )))
      }
      #saving to tiff
      else if (print2 == "tiff") {
        ggsave(
          paste0(destination_file_name, ".tiff"),
          sp,
          width = plot_width,
          height = plot_height
        )
        return(print(paste(
          "Stored in .tiff format at",
          getwd(),
          paste0(destination_file_name, ".tiff")
        )))
      }
      #saving to png
      else if (print2 == "png") {
        ggsave(
          paste0(destination_file_name, ".png"),
          sp,
          width = plot_width,
          height = plot_height
        )
        return(print(paste(
          "Stored in .png format at",
          getwd(),
          paste0(destination_file_name, ".png")
        )))
      }
      #saving to svg
      else if (print2 == "svg") {
        ggsave(
          paste0(destination_file_name, ".svg"),
          sp,
          width = plot_width,
          height = plot_height
        )
        return(print(paste(
          "Stored in .svg format at",
          getwd(),
          paste0(destination_file_name, ".svg")
        )))
      }
      else if (print2 == "ppt") {
        #dev.off()
        docc <- read_pptx()
        docc <- add_slide(docc, layout = "Title and Content", master = "Office Theme")
        docc <- ph_with_gg(x = docc, value = sp, type = "body", index = 1)
        print(docc, target = paste(destination_file_name,".pptx") )
      }
    }
  }
