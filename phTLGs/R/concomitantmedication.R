#' Generate table for Concomitant Medication
#'
#' This function generates table for Concomitant Medication
#' @param data Must be dataframe following CDISC data format containing data related to Concomitant medications
#' @param patientsData Must be dataframe containing data for different patients, one row per patient
#' @param medication Case sensitive name of the column in dataset which contains medications
#' @param treatmentGroup Case sensitive name of the column in dataset which contains treatment groups
#' @param subjId Case sensitive name of the column in dataset which contains unique subject IDs of the patients
#' @param sortingGroup The name of the treatment group which will be used to sort the final results based on frequencies
#' @param print2 Helps in getting the table printed to word document or pdf or excel.
#'               Just assign value to 'doc' for word document and 'pdf' for pdf and 'excel' for excel.
#'               If nothing entered then only dataframe is returned
#' @param filePath Path of file to which table is to be exported
#' @param tableStyle Option to style generated table in the document. Three possible themes: 1) no-shading: plain table, 2) zebra-gray: zebra theme with gray color, 3) zebra-blue: zebra theme with blue color
#' @param docTitles Titles to be used in exported file. It must be a vector
#' @param footNotes Foot Notes to be added below table in exported file. It must be a vector
#' @return A dataframe or exported file path
#' @author Baqir (baqir@venturedive.com)
#' @import dplyr
#' @import lazyeval
#' @import tidyr
#' @import htmltools
#' @import officer
#' @import flextable
#' @export
con_meds <- function(data, patientsData, medication, treatmentGroup, subjId, sortingGroup = "Placebo", print2 = c("doc"),
    filePath, tableStyle = "no-shading", docTitles = c(), footNotes = c()) {

    print(helpers.getUserInfo())
    # checks before processing further
    validationErrors <- c()

    if (missing(medication)) {
        validationErrors = c(validationErrors, "Column name for medication is required")
    } else if (!medication %in% colnames(data)) {
        validationErrors = c(validationErrors, paste(medication, "column does not exist in the provided dataset"))
    }

    if (missing(treatmentGroup)) {
        validationErrors = c(validationErrors, "Column name for treatment group is required")
    } else if (!treatmentGroup %in% colnames(patientsData)) {
        validationErrors = c(validationErrors, paste(treatmentGroup, "column does not exist in the provided dataset"))
    }

    if (missing(subjId)) {
        validationErrors = c(validationErrors, "Column name for Subject ID is required")
    } else if (!subjId %in% colnames(data)) {
        validationErrors = c(validationErrors, paste(subjId, "column does not exist in the provided dataset"))
    }

    if (length(validationErrors) > 0) {
        # If any validation error occurs, return from here
        return(validationErrors)
    }

    finalFrame <- data.frame()
    # getting unique treatment groups from data set
    uniqueTreatmentGroups <- unique(patientsData[[treatmentGroup]])

    # this variable will be used for final headers in flextable
    headerLabels <- list()
    headerLabels[[medication]] <- "Concomitant Medication"

    # vars work
    grpCols <- c(medication, treatmentGroup)

    dotsForGroupBy <- lapply(grpCols, as.symbol)

    groupedData <- data %>% dplyr::inner_join(patientsData, by = c(subjId)) %>%
        dplyr::group_by_(.dots = dotsForGroupBy) %>% dplyr::summarise_(unique_subjects = lazyeval::interp(~dplyr::n_distinct(var),
        var = as.name(subjId)))

    finalFrame <- tidyr::spread_(groupedData, treatmentGroup, "unique_subjects")

    # make sure that its a dataframe
    finalFrame <- as.data.frame(finalFrame)
    finalFrame[is.na(finalFrame)] <- 0

    oldNames <- uniqueTreatmentGroups
    newNames <- c("PL", "T2", "T1")
    treatmentGroupNameMapping <- data.frame(oldNames, newNames)

    # renaming columns
    existing <- match(oldNames, names(finalFrame))
    names(finalFrame)[na.omit(existing)] <- newNames[which(!is.na(existing))]

    # sorting work - start

    matched <- treatmentGroupNameMapping %>% dplyr::filter(oldNames == sortingGroup)
    sortingGroupFormatted <- as.character(matched$newNames)

    finalFrame <- finalFrame %>%
      dplyr::arrange_(.dots = lazyeval::interp(~ desc(n1), n1 = as.name(sortingGroupFormatted)))

    # sorting work - end
    # percentages work
    for (k in 1:length(uniqueTreatmentGroups)) {
        currentGroup <- as.character(uniqueTreatmentGroups[k])

        matched <- treatmentGroupNameMapping %>% dplyr::filter(oldNames == currentGroup)
        currentGroupFormatted <- as.character(matched$newNames)

        headerLabels[[currentGroupFormatted]] <- paste(currentGroupFormatted,
            " \r\n(N=", sum(finalFrame[[currentGroupFormatted]]), ") \r\nn (%)",
            sep = "")

        finalFrame <- finalFrame %>% dplyr::mutate_(.dots = setNames(list(lazyeval::interp(quote(paste(var1,
            " (", round((as.numeric(var1)/sum(var1)) * 100, 2), " %)", sep = "")),
            var1 = as.name(currentGroupFormatted))), currentGroupFormatted))
    }

    if (missing(print2)) {
        return(finalFrame)
    } else if (print2 == "doc") {
        # HTML EScaping to handle special characters
        finalFrame <- htmltools::htmlEscape(finalFrame)

        widths <- vector(mode = "numeric", length = ncol(finalFrame))

        for (i in 1:length(widths)) {
          widths[i] <- 2.0
        }

        flexTableObj <- flextable::flextable(data = finalFrame) %>% flextable::fontsize(part = "body",
            size = 9) %>% flextable::width(width = widths)

        headerLabels[["x"]] <- flexTableObj
        flexTableObj <- do.call(flextable::set_header_labels, headerLabels)
        flexTableObj <- helpers.themeFlextable(flexTableObj, tableStyle)

        # center align
        flexTableObj <- flexTableObj %>% flextable::align(align = "center", part = "all")
        doc <- helpers.getWordDoc(flexTableObj, docTitles, footNotes)
        print(doc, target = filePath)
    } else if (print2 == "pdf") {
        finalFrame <- helpers.renameDataFrameColumns(finalFrame, headerLabels)
        helpers.writePdf(finalFrame, filePath, docTitles, footNotes)
        print(filePath)
    }
    else if (print2 == "excel") {
        finalFrame <- helpers.renameDataFrameColumns(finalFrame, headerLabels)
        helpers.writeToExcel(finalFrame, filePath, docTitles, footNotes)
        print(filePath)
    }
    else if (print2 == "ppt") {
      a = 1
      doc <- read_pptx()
      while(a <= nrow(finalFrame)){
        if((a+14) < nrow(finalFrame)){
          ft <- flextable(finalFrame[a:(a+14),])
        }
        else{
          ft <- flextable(finalFrame[a:nrow(finalFrame),])
        }
        ft <- width(ft, width = 1.25)
        ft <- width(ft, j ='cmdecod', width = 3)
        doc <- add_slide(doc, layout = "Title and Content",master = "Office Theme")
        doc <- ph_with_flextable(doc, value = ft, type = "body")
        a <- (a+15)
      }
      print(doc, target = "conco.pptx")
    }
}