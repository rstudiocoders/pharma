#' This function exports data to word, excel or pdf format along with titles and footnotes
#'
#' @param data Data to be exported
#' @param vars Variables that are required in the listing, will include all the variables if not specified.
#' @param docTitles Titles to be used in exported file. It must be a vector.
#' @param footNotes Footnotes to be added below table in exported file. It must be a vector.
#' @param print2 Helps in getting the table printed to word document or pdf or excel.
#'               Just assign value to 'doc' for word document and 'pdf' for pdf and 'excel' for excel.
#'               If nothing entered then only dataframe is returned
#' @param filePath Path of file to which table is to be exported
#' @return Exported file path
#' @author Baqir (baqir@venturedive.com)
#' @author Alan Hopkins ahopkins@BioQuids.com
#' @import magrittr
#' @import htmltools
#' @import officer
#' @import flextable
#' @export
datalist <-
  function(data,
           vars = c(),
           docTitles = c(),
           footNotes = c(),
           print2 = c('doc',"pdf","excel"),
           filePath) {
    finalDataToWrite <- data

    if (length(vars) > 0) {
      finalDataToWrite <- data %>% dplyr::select(one_of(vars))
    }

    if (missing(print2)) {
      return(finalDataToWrite)
    }

    if (print2 == "doc") {
      # HTML EScaping to handle special characters
      finalDataToWrite <- htmltools::htmlEscape(finalDataToWrite)

      widths <-
        vector(mode = "numeric", length = ncol(finalDataToWrite))

      for (i in 1:length(widths)) {
        widths[i] <- 1.5
      }

      flexTableObj <-
        flextable::flextable(data = finalDataToWrite) %>% 
        flextable::fontsize(part = "body", size = 9) %>% 
        flextable::width(width = widths)

      flexTableObj <-
        helpers.themeFlextable(flexTableObj, 'no-shading')

      # center align
      flexTableObj <-
        flexTableObj %>% flextable::align(align = "center", part = "all")

      doc <- helpers.getWordDoc(flexTableObj, docTitles, footNotes)

      print(doc, target = filePath)
    } else if (print2 == "pdf") {
      helpers.writePdf(finalDataToWrite, filePath, docTitles, footNotes)
      print(filePath)
    }
    else if (print2 == "excel") {
      helpers.writeToExcel(finalDataToWrite, filePath, docTitles, footNotes)
      print(filePath)
    }

  }
