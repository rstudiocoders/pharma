---
title: "Graphs"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Graphs}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(fig.width=9, fig.height=4)
library(phTLGs)
```

## lf_trellis Graph
A set of scatterplots of the maximum value against baseline over the treatment period with a panel for each LFT measurement and a symbol for each treatment group. It is important to use identical scaling on the x- and y-axis. Taking "adlbc" data which is already part of phTLGs sample datasets

```{r lf_trellis1}
lf_trellis(data = adlbc,inc_col = "LBTESTCD",inc_val =c("AST","ALT","BILI"),exc_col ="VISIT",exc_val = "UNSCHEDULED",inc_val3 = "LAB BASELINE",lb_val = "LBSTRESN" ,subjids = "USUBJID", treatment = "TRTP",blhi = "BLHI",xlabel = "this is x-axis")
```

Scales on y- axis and x-axis can be specified, using x_scale and y_scale.
The output could be printed to different devices. Using print2.
The destination file name could be specified. using destination_file_name.
By default working directory shall be used to store your printed files.
For a word document, it could be saved in already existing document. Just use doc_template to give path of existing document.

```{r lf_trellis2}
lf_trellis(data = adlbc,inc_col = "LBTESTCD",inc_val =c("AST","ALT","BILI"),exc_col ="VISIT",exc_val = "UNSCHEDULED",inc_val3 = "LAB BASELINE",lb_val = "LBSTRESN" ,subjids = "USUBJID", treatment = "TRTP",blhi = "BLHI",xlabel = "this is x-axis",x_scale = "log",y_scale = "log",print2 = "png",destination_file_name = "sample_trellis")
```

## Scatter plot


```{r pressure, echo=FALSE ,fig.align= "center",fig.width=12,fig.height=7}
lf_tests(adlbc,"LBTESTCD",c("AST","ALP","ALT","BILI"),"TRTP","USUBJID","LBSTRESN","BLHI",y_scale = "log10", x_scale = "log10")
```


## Boxplot max

```{r,fig.width=12,fig.height=7}
boxplot_max(data  =adlbc,test_col = "LBTESTCD",test_col_val = c("AST","ALP","ALT", "BILI", "GGT"), visit_col = "VISIT",visit_col_val_exc = c("UNSCHEDULED", "LAB BASELINE"),treatment = "TRTP" , subjids = "USUBJID", test_labval = "LBSTRESN",blhi = "BLHI", xlabel = "Tests", y_scale = "log",footnote = "ndjnej")
```

## Boxplot Visits

```{r,fig.width=12,fig.height=7}
boxplot_visits(data =adlbc,"LBTESTCD","ALT","VISIT",visit_col_val_exc = "UNSCHEDULED",test_lab_val = "LBSTRESN",blhi = "BLHI",treatment = "TRTPCD",subjids = "USUBJID",baseline_results = "BLSTRESN",footnote ="Jfi",replace_from = "LAB BASELINE", replace_to = "WEEK 0",fac_seq = c(0,27,2))
```

## Adverse event dot plot

```{r,fig.width=12,fig.height=7}
ae_dotplot_rr(data = adae,effect_col = "aedecod",treatment_col = "trtp",subjids ="usubjid" ,drug =  c("Placebo","Xanomeline High Dose"),max_No_AE = 10,page_nos = 3,conf_interval = 0.95,right_plot_title = "RR with 95% CI",left_plot_title = "Percent",footnote = "footnote here")
```

## Boxplot with stats
```{r,fig.width=12,fig.height=7}
boxplot_stats(data = adlbc,lab_col="LBTESTCD",lab_col_val="ALT",visit_col="VISIT",visit_col_val_exc="UNSCHEDULED",test_lab_val = "LBSTRESN",blhi = "BLHI",treatment = "TRTPCD",baseline_results = "BLSTRESN",subjid = "SUBJID",max_per_page =12,anfl="EFFICACY",anfl.value = "Y" ,saffl ="SAFETY" ,saffl.value = "Y",replace_from = "LAB BASELINE", replace_to = "WEEK 0")
```

## boxplot change

```{r,fig.width=12,fig.height=7}
boxplot_change(data = advs,base = "BASE",param_col = "PARAM",param_val = "Diastolic Blood Pressure (mmHg)", chg_col = "CHG",treatment = "TRTP",visit_col = "AVISIT",opt_atpt = "ATPT",opt_atpt_val="AFTER LYING DOWN FOR 5 MINUTES",subjid = "USUBJID", x_axis_test_val = c("Week 2","Week 4","Week 6","Week 8","Week 10", "Week 12","Week 14","Week 16","Week 18","Week 20","Week 22","Week 24","Week 26", "End of Treatment"),footnote = "fvfv",page_nos = 2)
```

## Time to event plot

```{r,fig.width=12,fig.height=7}
tte_plot(data=adtte,val="AVAL",cens="CNSR",treatment="TRTP",footnote = "here is the footnote")
```

## Forest plot

```{r,fig.width=12,fig.height=7}
forest_plot(
  data = adtte,
  treatment_col = "TRTA",
  drug = c("Placebo", "Xanomeline High Dose"),
  groups = c("AGEGR1N", "SEX", "RACE", "RACEN", "AGEGR1"),
  duration = "TRTDUR",
  censor = "CNSR",
  footnote = "this is a footnote."
)
```


