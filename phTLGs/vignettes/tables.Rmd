
<!-- --- -->
<!-- title: "phTLGs Tables Documentation" -->
<!-- output: rmarkdown::html_vignette -->
<!-- vignette: > -->
<!--   %\VignetteIndexEntry{phTLGs Tables Documentation} -->
<!--   %\VignetteEngine{knitr::rmarkdown} -->
<!--   \usepackage[utf8]{inputenc} -->
<!-- --- -->

<!-- ```{r setup, include=FALSE} -->
<!-- knitr::opts_chunk$set(echo = TRUE) -->
<!-- library(phTLGs) -->
<!-- ``` -->


<!-- ## Demographics DataFrame -->

<!-- This function will return demographics dataframe -->

<!-- ```{r demogframe, warning=F} -->
<!-- demog_df <- demog(data = adsl3,vars = c('sex', 'age', 'agegr1', 'race', 'ethnic','weightbl'),labels = c('Sex', 'Age', 'Age Categories', 'Race', 'Ethnicity', 'Weight'), treatmentGroup = 'trt01p', subjId = 'usubjid') -->
<!-- ``` -->

<!-- ## Demographics word file export -->

<!-- Exporting demographics dataframe to a word file -->

<!-- ```{r demogword, warning=F} -->
<!-- demog(data = adsl3, -->
<!--        vars = c('sex', 'age', 'agegr1', 'race', 'ethnic', 'weightbl'), -->
<!--        labels = c('Sex', 'Age', 'Age Categories', 'Race', 'Ethnicity', 'Weight'), -->
<!--        treatmentGroup = 'trt01p', -->
<!--        subjId = 'usubjid', -->
<!--        print2 = 'doc', -->
<!--        filePath = paste(getwd(), '/demographics.docx', sep=""), -->
<!--        tableStyle = 'zebra-blue', -->
<!--        docTitles = c( -->
<!--           'Title 1', -->
<!--           'Title 2' -->
<!--        ), -->
<!--        footNotes = c( -->
<!--           'Footnote 1', -->
<!--           'Footnote 2' -->
<!--        ) -->
<!-- ) -->
<!-- ``` -->

<!-- ## Demographics pdf file export -->

<!-- Exporting demographics dataframe to a pdf file -->

<!-- ```{r demogpdf, warning=F} -->
<!-- demog(data = adsl3, -->
<!--       vars = c('sex', 'age', 'agegr1', 'race', 'ethnic', 'weightbl'), -->
<!--       labels = c('Sex', 'Age', 'Age Categories', 'Race', 'Ethnicity', 'Weight'), -->
<!--       treatmentGroup = 'trt01p', -->
<!--       subjId = 'usubjid', -->
<!--       print2 = 'pdf', -->
<!--       filePath = paste(getwd(), '/demographics-2.pdf', sep=""), -->
<!--       docTitles = c( -->
<!--         'Title 1', -->
<!--         'Title 2' -->
<!--       ), -->
<!--       footNotes = c( -->
<!--         'Footnote 1', -->
<!--         'Footnote 2' -->
<!--       ) -->
<!-- ) -->
<!-- ``` -->

<!-- ## Demographics excel file export -->

<!-- Exporting demographics dataframe to an excel file -->

<!-- ```{r demogexcel, warning=F} -->
<!-- demog(data = adsl3, -->
<!--       vars = c('sex', 'age', 'agegr1', 'race', 'ethnic', 'weightbl'), -->
<!--       labels = c('Sex', 'Age', 'Age Categories', 'Race', 'Ethnicity', 'Weight'), -->
<!--       treatmentGroup = 'trt01p', -->
<!--       subjId = 'usubjid', -->
<!--       print2 = 'excel', -->
<!--       filePath = paste(getwd(), '/demographics-new.xlsx', sep=""), -->
<!--       docTitles = c( -->
<!--         'Title 1', -->
<!--         'Title 2' -->
<!--       ), -->
<!--       footNotes = c( -->
<!--         'Footnote 1', -->
<!--         'Footnote 2' -->
<!--       ) -->
<!-- ) -->
<!-- ``` -->


<!-- ## Concomitant Medication DataFrame -->

<!-- This function will return Concomitant Medication dataframe -->

<!-- ```{r cmframe, warning=F} -->
<!-- con_meds_df <- con_meds(data = cm,patientsData = adsl3,medication = 'cmdecod', treatmentGroup = 'trt01p',subjId = 'usubjid') -->
<!-- ``` -->

<!-- ## Concomitant Medication DataFrame Word file export -->

<!-- Exporting Concomitant Medication dataframe to a word file -->

<!-- ```{r cmword, warning=F} -->
<!-- con_meds( -->
<!--   data = cm, -->
<!--   patientsData = adsl3, -->
<!--   medication = 'cmdecod',  -->
<!--   treatmentGroup = 'trt01p', -->
<!--   subjId = 'usubjid', -->
<!--   print2 = 'doc', -->
<!--   filePath = paste(getwd(), '/medication.docx', sep=""), -->
<!--   tableStyle = 'no-shading', -->
<!--   docTitles = c( -->
<!--     'Title 1: Concimitant Medication', -->
<!--     'Title 2: This is second title' -->
<!--   ), -->
<!--   footNotes = c( -->
<!--     'Footnote 1', -->
<!--     'Footnote 2' -->
<!--   )) -->
<!-- ``` -->


<!-- ## Concomitant Medication DataFrame PDF file export -->

<!-- Exporting Concomitant Medication dataframe to a PDF file -->

<!-- ```{r cmpdf, warning=F} -->
<!-- con_meds(data = cm, -->
<!--          patientsData = adsl3, -->
<!--          medication = 'cmdecod', -->
<!--          treatmentGroup = 'trt01p', -->
<!--          subjId = 'usubjid', -->
<!--          print2 = 'pdf', -->
<!--          filePath = paste(getwd(), '/medication-2.pdf', sep=""), -->
<!--          docTitles = c( -->
<!--            'Title 1', -->
<!--            'Title 2' -->
<!--          ), -->
<!--          footNotes = c( -->
<!--            'Footnote 1', -->
<!--            'Footnote 2' -->
<!--          ) -->
<!-- ) -->
<!-- ``` -->


<!-- ## Concomitant Medication DataFrame excel file export -->

<!-- Exporting Concomitant Medication dataframe to excel file -->

<!-- ```{r cmexcel, warning=F} -->
<!-- con_meds(data = cm, -->
<!--          patientsData = adsl3, -->
<!--          medication = 'cmdecod', -->
<!--          treatmentGroup = 'trt01p', -->
<!--          subjId = 'usubjid', -->
<!--          print2 = 'excel', -->
<!--          filePath = paste(getwd(), '/medication.xlsx', sep=""), -->
<!--          docTitles = c( -->
<!--            'Title 1', -->
<!--            'Title 2' -->
<!--          ), -->
<!--          footNotes = c( -->
<!--            'Footnote 1', -->
<!--            'Footnote 2' -->
<!--          ) -->
<!-- ) -->
<!-- ``` -->

<!-- ## Disposition Dataframe -->

<!-- This function will return dispoition dataframe -->

<!-- ```{r dispositionframe, warning=F} -->
<!-- disposition_df <- disposition(data = adsl3, disposition = 'dcdecod', treatmentGroup = 'trt01p', subjId = 'usubjid') -->
<!-- ``` -->

<!-- ## Disposition DataFrame Word file export -->

<!-- Exporting Disposition dataframe to a word file -->

<!-- ```{r dispositionword, warning=F} -->
<!-- disposition(data = adsl3, -->
<!--             disposition = 'dcdecod', -->
<!--             treatmentGroup = 'trt01p', -->
<!--             subjId = 'usubjid', -->
<!--             print2 = 'doc', -->
<!--             filePath = paste(getwd(), '/disposition.docx', sep=""), -->
<!--             tableStyle = 'zebra-blue', -->
<!--             docTitles = c('Disposition Table'), -->
<!--             footNotes = c('First Footnote') -->
<!--            ) -->
<!-- ``` -->


<!-- ## Disposition DataFrame pdf file export -->

<!-- Exporting Disposition dataframe to pdf file -->

<!-- ```{r dispositionpdf, warning=F} -->
<!-- disposition(data = adsl3, -->
<!--             disposition = 'dcdecod', -->
<!--             treatmentGroup = 'trt01p', -->
<!--             subjId = 'usubjid', -->
<!--             print2 = 'pdf', -->
<!--             filePath = paste(getwd(), '/disposition.pdf', sep=""), -->
<!--             docTitles = c('Disposition Table', 'Title 2', 'Title 3', 'Title 4', 'Title 5'), -->
<!--             footNotes = c('Footnote1', 'Footnote2', 'Footnote 3', 'Footnote 4', 'Footnote 5') -->
<!-- ) -->
<!-- ``` -->


<!-- ## Disposition DataFrame excel file export -->

<!-- Exporting Disposition dataframe to excel file -->

<!-- ```{r dispositionexcel, warning=F} -->
<!-- disposition(data = adsl3, -->
<!--             disposition = 'dcdecod', -->
<!--             treatmentGroup = 'trt01p', -->
<!--             subjId = 'usubjid', -->
<!--             print2 = 'excel', -->
<!--             filePath = paste(getwd(), '/exported-docs/excel/disposition.xlsx', sep=""), -->
<!--             docTitles = c( -->
<!--               'Title 1', -->
<!--               'Title 2' -->
<!--             ), -->
<!--             footNotes = c( -->
<!--               'Footnote 1', -->
<!--               'Footnote 2' -->
<!--             ) -->
<!--             ) -->
<!-- ``` -->

<!-- ## Medical History Dataframe -->

<!-- This function will return Medical History dataframe -->

<!-- ```{r medhxframe, warning=F} -->
<!-- med_hx_df <- med_hx(data = mh, patientsData = adsl3, preferredTerm = 'mhdecod', treatmentGroup = 'trt01p', subjId = 'usubjid', systemOrganClass = 'mhbodsys') -->
<!-- ``` -->


<!-- ## Medical History DataFrame word file export -->

<!-- Exporting Medical History dataframe to word file -->

<!-- ```{r medhxword, warning=F} -->
<!-- med_hx(data = mh, -->
<!--        patientsData = adsl3, -->
<!--        preferredTerm = 'mhdecod', -->
<!--        treatmentGroup = 'trt01p', -->
<!--        subjId = 'usubjid', -->
<!--        systemOrganClass = 'mhbodsys', -->
<!--        print2 = 'doc', -->
<!--        filePath = paste(getwd(), '/medical-history.docx', sep=""), -->
<!--        tableStyle = 'no-shading', -->
<!--        docTitles = c( -->
<!--          'Title 1: Medical History', -->
<!--          'Title 2: This is second title' -->
<!--        ), -->
<!--        footNotes = c( -->
<!--          'Footnote 1', -->
<!--          'Footnote 2' -->
<!--        ) -->
<!--       ) -->
<!-- ``` -->

<!-- ## Medical History DataFrame pdf file export -->

<!-- Exporting Medical History dataframe to pdf file -->

<!-- ```{r medhxpdf, warning=F} -->
<!-- med_hx(data = mh, -->
<!--        patientsData = adsl3, -->
<!--        preferredTerm = 'mhdecod', -->
<!--        treatmentGroup = 'trt01p', -->
<!--        subjId = 'usubjid', -->
<!--        systemOrganClass = 'mhbodsys', -->
<!--        print2 = 'pdf', -->
<!--        filePath = paste(getwd(), '/medical-history.pdf', sep=""), -->
<!--        docTitles = c( -->
<!--          'Title 1', -->
<!--          'Title 2' -->
<!--        ), -->
<!--        footNotes = c( -->
<!--          'Footnote 1', -->
<!--          'Footnote 2' -->
<!--        ) -->
<!-- ) -->
<!-- ``` -->


<!-- ## Medical History DataFrame excel file export -->

<!-- Exporting Medical History dataframe to excel file -->

<!-- ```{r medhxexcel, warning=F} -->
<!-- med_hx(data = mh, -->
<!--        patientsData = adsl3, -->
<!--        preferredTerm = 'mhdecod', -->
<!--        treatmentGroup = 'trt01p', -->
<!--        subjId = 'usubjid', -->
<!--        systemOrganClass = 'mhbodsys', -->
<!--        print2 = 'excel', -->
<!--        filePath = paste(getwd(), '/medical-history.xlsx', sep=""), -->
<!--        docTitles = c( -->
<!--          'Title 1', -->
<!--          'Title 2' -->
<!--        ), -->
<!--        footNotes = c( -->
<!--          'Footnote 1', -->
<!--          'Footnote 2' -->
<!--        ) -->
<!-- ) -->
<!-- ``` -->

<!-- ## Adverse Event with Treatment Group Dataframe -->

<!-- This function will return Adverse Event with Treatment Group dataframe -->

<!-- ```{r ae_tab_txframe, warning=F} -->
<!-- ae_tab_tx_df <- ae_tab_tx(data = adae, confidenceInterval = 0.95, preferredTerm = 'aedecod', treatmentGroup = 'trtp', subjId = 'usubjid', controlGroup = 'Placebo', computeRiskDifferences = TRUE, minRelativeFrequency = 1.5) -->
<!-- ``` -->

<!-- ## Adverse Event with Treatment Group Dataframe word file export -->

<!-- Exporting Adverse Event with Treatment Group dataframe to word file -->

<!-- ```{r ae_tab_txword, warning=F} -->
<!-- ae_tab_tx( -->
<!--         data = adae, -->
<!--         confidenceInterval = 0.95, -->
<!--         preferredTerm = 'aedecod', -->
<!--         treatmentGroup = 'trtp', -->
<!--         subjId = 'usubjid', -->
<!--         controlGroup = 'Placebo', -->
<!--         computeRiskDifferences = TRUE, -->
<!--         minRelativeFrequency = 2.0, -->
<!--         print2 = 'doc', -->
<!--         filePath = paste(getwd(), '/ae_tab_tx.docx', sep=""), -->
<!--         tableStyle = 'no-shading', -->
<!--         docTitles = c( -->
<!--           'Title 1', -->
<!--           'Title 2' -->
<!--         ), -->
<!--         footNotes = c( -->
<!--           'Footnote 1', -->
<!--           'Footnote 2' -->
<!--         ) -->
<!-- ) -->
<!-- ``` -->

<!-- ## Adverse Event with Treatment Group Dataframe pdf file export -->

<!-- Exporting Adverse Event with Treatment Group dataframe to pdf file -->

<!-- ```{r ae_tab_txpdf, warning=F} -->
<!-- ae_tab_tx( -->
<!--   data = adae, -->
<!--   confidenceInterval = 0.95, -->
<!--   preferredTerm = 'aedecod', -->
<!--   treatmentGroup = 'trtp', -->
<!--   subjId = 'usubjid', -->
<!--   controlGroup = 'Placebo', -->
<!--   print2 = 'pdf', -->
<!--   filePath = paste(getwd(), '/ae_tab_tx.pdf', sep=""), -->
<!--   docTitles = c( -->
<!--     'Title 1', -->
<!--     'Title 2' -->
<!--   ), -->
<!--   footNotes = c( -->
<!--     'Footnote 1', -->
<!--     'Footnote 2' -->
<!--   ) -->
<!-- ) -->
<!-- ``` -->


<!-- ## Adverse Event with Treatment Group Dataframe excel file export -->

<!-- Exporting Adverse Event with Treatment Group dataframe to excel file -->

<!-- ```{r ae_tab_txexcel, warning=F} -->
<!-- ae_tab_tx( -->
<!--   data = adae, -->
<!--   confidenceInterval = 0.95, -->
<!--   preferredTerm = 'aedecod', -->
<!--   treatmentGroup = 'trtp', -->
<!--   subjId = 'usubjid', -->
<!--   controlGroup = 'Placebo', -->
<!--   print2 = 'excel', -->
<!--   filePath = paste(getwd(), '/ae_tab_tx.xlsx', sep=""), -->
<!--   docTitles = c( -->
<!--     'Title 1', -->
<!--     'Title 2' -->
<!--   ), -->
<!--   footNotes = c( -->
<!--     'Footnote 1', -->
<!--     'Footnote 2' -->
<!--   ) -->
<!-- ) -->
<!-- ``` -->

<!-- ## System Organ Class and Adverse Event with Treatment Group Dataframe -->

<!-- This function will return System Organ Class and Adverse Event with Treatment Group dataframe -->

<!-- ```{r ae_sog_txframe, warning=F} -->
<!-- ae_sog_tx_df <- ae_sog_tx(data = adae, confidenceInterval = 0.95, preferredTerm = 'aedecod', treatmentGroup = 'trtp', subjId = 'usubjid', systemOrganClass = 'aebodsys', controlGroup = 'Placebo', computeRiskDifferences = TRUE, minRelativeFrequency = 1.5) -->
<!-- ``` -->


<!-- ## System Organ Class and Adverse Event with Treatment Group dataframe word file export -->

<!-- Exporting System Organ Class and Adverse Event with Treatment Group dataframe to word file -->

<!-- ```{r ae_sog_txword, warning=F} -->
<!-- ae_sog_tx( -->
<!--             data = adae, -->
<!--             confidenceInterval = 0.95, -->
<!--             preferredTerm = 'aedecod', -->
<!--             treatmentGroup = 'trtp', -->
<!--             subjId = 'usubjid', -->
<!--             systemOrganClass = 'aebodsys', -->
<!--             controlGroup = 'Placebo', -->
<!--             computeRiskDifferences = TRUE, -->
<!--             print2 = 'doc', -->
<!--             filePath = paste(getwd(), '/ae_sog_tx.docx', sep=""), -->
<!--             tableStyle = 'no-shading', -->
<!--             docTitles = c( -->
<!--               'Title 1', -->
<!--               'Title 2' -->
<!--             ), -->
<!--             footNotes = c( -->
<!--               'Footnote 1', -->
<!--               'Footnote 2' -->
<!--             ) -->
<!-- ) -->
<!-- ``` -->


<!-- ## System Organ Class and Adverse Event with Treatment Group dataframe pdf file export -->

<!-- Exporting System Organ Class and Adverse Event with Treatment Group dataframe to pdf file -->

<!-- ```{r ae_sog_txpdf, warning=F} -->
<!-- ae_sog_tx( -->
<!--   data = adae, -->
<!--   confidenceInterval = 0.95, -->
<!--   preferredTerm = 'aedecod', -->
<!--   treatmentGroup = 'trtp', -->
<!--   subjId = 'usubjid', -->
<!--   systemOrganClass = 'aebodsys', -->
<!--   controlGroup = 'Placebo', -->
<!--   print2 = 'pdf', -->
<!--   filePath = paste(getwd(), '/ae_sog_tx.pdf', sep=""), -->
<!--   docTitles = c( -->
<!--     'Title 1', -->
<!--     'Title 2' -->
<!--   ), -->
<!--   footNotes = c( -->
<!--     'Footnote 1', -->
<!--     'Footnote 2' -->
<!--   ) -->
<!-- ) -->
<!-- ``` -->


<!-- ## System Organ Class and Adverse Event with Treatment Group dataframe excel file export -->

<!-- Exporting System Organ Class and Adverse Event with Treatment Group dataframe to excel file -->

<!-- ```{r ae_sog_txexcel, warning=F} -->
<!-- ae_sog_tx( -->
<!--   data = adae, -->
<!--   confidenceInterval = 0.95, -->
<!--   preferredTerm = 'aedecod', -->
<!--   treatmentGroup = 'trtp', -->
<!--   subjId = 'usubjid', -->
<!--   systemOrganClass = 'aebodsys', -->
<!--   controlGroup = 'Placebo', -->
<!--   print2 = 'excel', -->
<!--   filePath = paste(getwd(), '/ae_sog_tx.xlsx', sep=""), -->
<!--   docTitles = c( -->
<!--     'Title 1', -->
<!--     'Title 2' -->
<!--   ), -->
<!--   footNotes = c( -->
<!--     'Footnote 1', -->
<!--     'Footnote 2' -->
<!--   ) -->
<!-- ) -->
<!-- ``` -->

<!-- ## Adverse Event and Severity with Treatment Group Dataframe -->

<!-- This function will return Adverse Event and Severity with Treatment Group dataframe -->

<!-- ```{r ae_tab_sev_txframe, warning=F} -->
<!-- ae_tab_sev_tx_df <- ae_tab_sev_tx(data = adae, confidenceInterval = 0.95, preferredTerm = 'aedecod', treatmentGroup = 'trtp', subjId = 'usubjid', systemOrganClass = 'aebodsys', severity = 'aesev', controlGroup = 'Placebo', computeRiskDifferences = TRUE, minRelativeFrequency = 1.5) -->
<!-- ``` -->


<!-- ## Adverse Event and Severity with Treatment Group Dataframe word file export -->

<!-- Exporting Adverse Event and Severity with Treatment Group Dataframe to word file -->

<!-- ```{r ae_tab_sev_txword, warning=F} -->
<!-- ae_tab_sev_tx( -->
<!--               data = adae, -->
<!--               confidenceInterval = 0.95, -->
<!--               preferredTerm = 'aedecod', -->
<!--               treatmentGroup = 'trtp', -->
<!--               subjId = 'usubjid', -->
<!--               systemOrganClass = 'aebodsys', -->
<!--               severity = 'aesev', -->
<!--               controlGroup = 'Placebo', -->
<!--               computeRiskDifferences = FALSE, -->
<!--               minRelativeFrequency = 4.0, -->
<!--               print2 = 'doc', -->
<!--               filePath = paste(getwd(), '/ae_tab_sev_tx.docx', sep=""), -->
<!--               tableStyle = 'zebra-blue', -->
<!--               docTitles = c( -->
<!--                 'Title 1', -->
<!--                 'Title 2' -->
<!--               ), -->
<!--               footNotes = c( -->
<!--                 'Footnote 1', -->
<!--                 'Footnote 2' -->
<!--               ) -->
<!-- ) -->

<!-- ``` -->


<!-- ## Adverse Event and Severity with Treatment Group Dataframe pdf file export -->

<!-- Exporting Adverse Event and Severity with Treatment Group Dataframe to pdf file -->

<!-- ```{r ae_tab_sev_txpdf, warning=F} -->
<!-- ae_tab_sev_tx( -->
<!--   data = adae, -->
<!--   confidenceInterval = 0.95, -->
<!--   preferredTerm = 'aedecod', -->
<!--   treatmentGroup = 'trtp', -->
<!--   subjId = 'usubjid', -->
<!--   systemOrganClass = 'aebodsys', -->
<!--   severity = 'aesev', -->
<!--   controlGroup = 'Placebo', -->
<!--   print2 = 'pdf', -->
<!--   filePath = paste(getwd(), '/ae_tab_sev_tx.pdf', sep=""), -->
<!--   docTitles = c( -->
<!--     'Title 1', -->
<!--     'Title 2' -->
<!--   ), -->
<!--   footNotes = c( -->
<!--     'Footnote 1', -->
<!--     'Footnote 2' -->
<!--   ) -->
<!-- ) -->
<!-- ``` -->


<!-- ## Adverse Event and Severity with Treatment Group Dataframe excel file export -->

<!-- Exporting Adverse Event and Severity with Treatment Group Dataframe to excel file -->

<!-- ```{r ae_tab_sev_txexcel, warning=F} -->
<!-- ae_tab_sev_tx( -->
<!--   data = adae, -->
<!--   confidenceInterval = 0.95, -->
<!--   preferredTerm = 'aedecod', -->
<!--   treatmentGroup = 'trtp', -->
<!--   subjId = 'usubjid', -->
<!--   systemOrganClass = 'aebodsys', -->
<!--   severity = 'aesev', -->
<!--   controlGroup = 'Placebo', -->
<!--   print2 = 'excel', -->
<!--   filePath = paste(getwd(), '/ae_tab_sev_tx.xlsx', sep=""), -->
<!--   docTitles = c( -->
<!--     'Title 1', -->
<!--     'Title 2' -->
<!--   ), -->
<!--   footNotes = c( -->
<!--     'Footnote 1', -->
<!--     'Footnote 2' -->
<!--   ) -->
<!-- ) -->
<!-- ``` -->
