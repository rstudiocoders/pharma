\name{advs}
\alias{advs}
\docType{data}
\title{
CDISC:
}
\description{
CDISC: Vital Signs Analysis Dataset (ADVS)
}
\usage{phTLGs::advs}
\format{
  A data frame with 32139 observations on the following 44 variables.
  \describe{
 \item{\code{ STUDYID }}{ Study Identifier }
 \item{\code{ SITEID }}{ Study Site Identifier }
 \item{\code{ USUBJID }}{ Unique Subject Identifier }
 \item{\code{ AGE }}{ Age }
 \item{\code{ AGEGR1 }}{ Pooled Age Group 1 }
 \item{\code{ AGEGR1N }}{ Pooled Age Group 1 (N) }
 \item{\code{ RACE }}{ Race }
 \item{\code{ RACEN }}{ Race (N) }
 \item{\code{ SEX }}{ Sex }
 \item{\code{ SAFFL }}{ Safety Population Flag }
 \item{\code{ TRTSDT }}{ Date of First Exposure to Treatment }
 \item{\code{ TRTEDT }}{ Date of Last Exposure to Treatment }
 \item{\code{ TRTP }}{ Planned Treatment }
 \item{\code{ TRTPN }}{ Planned Treatment (N) }
 \item{\code{ TRTA }}{ Actual Treatment }
 \item{\code{ TRTAN }}{ Actual Treatment (N) }
 \item{\code{ PARAMCD }}{ Parameter Code }
 \item{\code{ PARAM }}{ Parameter }
 \item{\code{ PARAMN }}{ Parameter Number }
 \item{\code{ ADT }}{ Analysis Date }
 \item{\code{ ADY }}{ Analysis Relative Day }
 \item{\code{ ATPTN }}{ Analysis Timepoint (N) }
 \item{\code{ ATPT }}{ Analysis Timepoint }
 \item{\code{ AVISIT }}{ Analysis Visit }
 \item{\code{ AVISITN }}{ Analysis Visit (N) }
 \item{\code{ AVAL }}{ Analysis Value }
 \item{\code{ BASE }}{ Baseline Value }
 \item{\code{ CHG }}{ Change from Baseline }
 \item{\code{ PCHG }}{ Percent Change from Baseline }
 \item{\code{ VISITNUM }}{ Visit Number }
 \item{\code{ VISIT }}{ Visit Name }
 \item{\code{ VSSEQ }}{ Sequence Number }
 \item{\code{ ANL01FL }}{ Analysis Record Flag 01 }
 \item{\code{ ABLFL }}{ Baseline Record Flag }
 \item{\code{ ANRLO }}{ Analysis Normal Range Lower Limit }
 \item{\code{ ANRHI }}{ Analysis Normal Range Upper Limit }
 \item{\code{ BNRIND }}{ Baseline Reference Range Indicator }
 \item{\code{ ANRIND }}{ Analysis Reference Range Indicator }
 \item{\code{ SHIFT1 }}{ Shift 1 }
 \item{\code{ CRIT1 }}{ Analysis Criterion 1 }
 \item{\code{ CRIT1FL }}{ Criterion 1 Evaluation Result Flag }
 \item{\code{ CRIT2 }}{ Analysis Criterion 2 }
 \item{\code{ CRIT2FL }}{ Criterion 2 Evaluation Result Flag }
 \item{\code{ CHGCAT1 }}{ Change from Baseline Category 1 }
  }
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
phTLGs::adtte
}
\keyword{datasets}
