\name{adae}
\alias{adae}
\docType{data}
\title{
CDISC: Adverse Event Analysis Dataset (ADAE)
}
\description{
Adverse Event Analysis Dataset (ADAE)
}
\usage{phTLGs::adae}
\format{
  A data frame with 1191 observations on the following 55 variables.
  \describe{
    \item{\code{USUBJID}}{Unique Subject Identifier}
    \item{\code{AESEQ}}{Sequence Number}
    \item{\code{STUDYID}}{Study Identifier}
    \item{\code{DOMAIN}}{Domain Abbreviation}
    \item{\code{SITEID}}{Study Site Identifier}
    \item{\code{SUBJID}}{Subject Identifier for Study}
    \item{\code{AETERM}}{Reported Term for AE}
    \item{\code{AEDECOD}}{Dictionary-Derived Term }
    \item{\code{AEBODSYS}}{Body System or Organ Class}
    \item{\code{AESEV}}{Severity/Intensity}
    \item{\code{AESER}}{Serious Event}
    \item{\code{AESERN}}{Serious Event, Numeric}
    \item{\code{AEREL}}{Causality}
    \item{\code{DSREASAE}}{Discontinued Due to AE? }
    \item{\code{ONTRTFL}}{On or Off Study Treatment Flag}
    \item{\code{TRTEMFL}}{	Treatment Emergent Flag}
    \item{\code{AEFN}}{1st AE Occurrence (1= Yes)}
    \item{\code{BODFN}}{1st Occurrence of SOC (1 = Yes)}
    \item{\code{DECODFN}}{	1st Occurrence of PT (1 = Yes)}
    \item{\code{SAEFN}}{	1st Serious AE Occurrence (1 = Yes)}
    \item{\code{SDECODFN}}{1st Serious PT Occurrence (1 = Yes)}
    \item{\code{SBODFN}}{	1st Serious SOC Occurrence (1 = Yes)}
    \item{\code{AEDERMFN}}{Special Dermatologic-Related Event (1 = Yes)}
    \item{\code{AESTDT}}{Start Date/Time of AE}
    \item{\code{AESTDTF}}{Imputed Date Flag}
    \item{\code{AESTDY}}{Study Day of Start of AE}
    \item{\code{ANLSTDY}}{Analysis Start Day}
    \item{\code{AEENDT}}{End Date/Time of AE}
    \item{\code{AEENDY}}{Study Day of End of AE}
    \item{\code{TRTP}}{ADaM Description of Planned Arm}
    \item{\code{TRTPCD}}{	ADaM Planned Arm Code}
    \item{\code{TRTPN}}{ADam Planned Arm Code, Numeric}
    \item{\code{AGE}}{Age in AGEU at RFSTDTC}
    \item{\code{AGEGRP}}{Age Group}
    \item{\code{AGEGRPN}}{Age Group, Numeric }
    \item{\code{RACE}}{Race}
    \item{\code{RACEN}}{Race, Numeric}
    \item{\code{SEX}}{Sex}
    \item{\code{SAFETY}}{	Safety Population Flag}
    \item{\code{ITT}}{Intent to Treat Population Flag}
    \item{\code{EFFICACY}}{Efficacy Population Flag}
    \item{\code{COMPLT24}}{Completer of Week 24 Population Flag}
    \item{\code{AEDICT}}{Coding Dictionary Information}
    \item{\code{AESCAN}}{Involves Cancer}
    \item{\code{AESCONG}}{Congenital Anomaly or Birth Defect}
    \item{\code{AESDISAB}}{Persist or Significant Disability/Incapacity}
    \item{\code{AESDTH}}{Results in Death}
    \item{\code{AESHOSP}}{Requires or Prolongs Hospitalization}
    \item{\code{AESLIFE}}{Is Life Threatening}
    \item{\code{HLGTERM}}{High Level Group Term}
    \item{\code{HLTERM}}{High Level Term}
    \item{\code{LLTERM}}{Lower Level Term}
    \item{\code{LSTDOSDT}}{Date of Last Dose}
    \item{\code{TRTDUR}}{Duration of Treatment}
    \item{\code{TRTSTDT}}{Start Date of Treatment}

  }
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
phTLGS::adae
}
\keyword{datasets}
