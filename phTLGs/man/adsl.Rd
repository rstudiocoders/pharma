\name{adsl}
\alias{adsl}
\docType{data}
\title{
CDISC: Subject Level Analysis Dataset (ADSL)
}
\description{
Subject Level Analysis Dataset (ADSL)
}
\usage{phTLGs::adsl}
\format{
  A data frame with 254 observations on the following 48 variables.
  \describe{
 \item{\code{ STUDYID }}{ Study Identifier }
 \item{\code{ USUBJID }}{ Unique Subject Identifier }
 \item{\code{ SUBJID }}{ Subject Identifier for the Study }
 \item{\code{ SITEID }}{ Study Site Identifier }
 \item{\code{ SITEGR1 }}{ Pooled Site Group 1 }
 \item{\code{ ARM }}{ Description of Planned Arm }
 \item{\code{ TRT01P }}{ Planned Treatment for Period 01 }
 \item{\code{ TRT01PN }}{ Planned Treatment for Period 01 (N) }
 \item{\code{ TRT01A }}{ Actual Treatment for Period 01 }
 \item{\code{ TRT01AN }}{ Actual Treatment for Period 01 (N) }
 \item{\code{ TRTSDT }}{ Date of First Exposure to Treatment }
 \item{\code{ TRTEDT }}{ Date of Last Exposure to Treatment }
 \item{\code{ TRTDURD }}{ Total Treatment Duration (Days) }
 \item{\code{ AVGDD }}{ Avg Daily Dose (as planned) }
 \item{\code{ CUMDOSE }}{ Cumulative Dose (as planned) }
 \item{\code{ AGE }}{ Age }
 \item{\code{ AGEGR1 }}{ Pooled Age Group 1 }
 \item{\code{ AGEGR1N }}{ Pooled Age Group 1 (N) }
 \item{\code{ AGEU }}{ Age Units }
 \item{\code{ RACE }}{ Race }
 \item{\code{ RACEN }}{ Race (N) }
 \item{\code{ SEX }}{ Sex }
 \item{\code{ ETHNIC }}{ Ethnicity }
 \item{\code{ SAFFL }}{ Safety Population Flag }
 \item{\code{ ITTFL }}{ Intent-To-Treat Population Flag }
 \item{\code{ EFFFL }}{ Efficacy Population Flag }
 \item{\code{ COMP8FL }}{ Completers of Week 8 Population Flag }
 \item{\code{ COMP16FL }}{ Completers of Week 16 Population Flag }
 \item{\code{ COMP24FL }}{ Completers of Week 24 Population Flag }
 \item{\code{ DISCONFL }}{ Subject Discontinued Study Flag }
 \item{\code{ DSRAEFL }}{ Subject Discontinued due to AE Flag }
 \item{\code{ DTHFL }}{ Subject Death Flag }
 \item{\code{ BMIBL }}{ Baseline BMI (kg/m^2) }
 \item{\code{ BMIBLGR1 }}{ Pooled Baseline BMI Group 1 }
 \item{\code{ HEIGHTBL }}{ Baseline Height (cm) }
 \item{\code{ WEIGHTBL }}{ Baseline Weight (kg) }
 \item{\code{ EDUCLVL }}{ Years of Education }
 \item{\code{ DISONDT }}{ Date of Onset of Disease }
 \item{\code{ DURDIS }}{ Duration of Disease (Months) }
 \item{\code{ DURDSGR1 }}{ Pooled Disease Duration Group 1 }
 \item{\code{ VISIT1DT }}{ Date of Visit 1 }
 \item{\code{ RFSTDTC }}{ Subject Reference Start Date/Time }
 \item{\code{ RFENDTC }}{ Subject Reference End Date/Time }
 \item{\code{ VISNUMEN }}{ End of Trt Visit (Vis 12 or Early Term.) }
 \item{\code{ RFENDT }}{ Date of Discontinuation/Completion }
 \item{\code{ DCDECOD }}{ Standardized Disposition Term }
 \item{\code{ DCSREAS }}{ Reason for Discontinuation from Study }
 \item{\code{ MMSETOT }}{ MMSE Total }
  }
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(adsl)
## maybe str(adsl) ; plot(adsl) ...
}
\keyword{datasets}
