\name{adlbc}
\alias{adlbc}
\docType{data}
\title{
CDISC: Chemistry Lab Analysis Data Set (ADLBC)
}
\description{
Chemistry Lab Analysis Data Set (ADLBC)
}
\usage{phTLGs::adlbc}
\format{
  A data frame with 58 variables.
  \describe{

    \item{\code{USUBJID}}{Unique Subect Identifer. Type: text}
    \item{\code{VISITNUM}}{Visit Number}
    \item{\code{LBTESTCD}}{Lab Test or Examination Short Name}
    \item{\code{STUDYID}}{Study Identifier Type: float}
    \item{\code{DOMAIN}}{Domain Abbreviation Type: float}
    \item{\code{SUBJID}}{Subject Identifier for the Study. Type: float}
    \item{\code{VISIT}}{Visit Name Type: float}
    \item{\code{ANLDY}}{Analysis Day Type: float}
    \item{\code{LBDT}}{Date of Specimen Collection}
    \item{\code{LBTYPE}}{Type of Lab Test (Hem, Chem)}
    \item{\code{LBTEST}}{Lab Test or Examination Name}
    \item{\code{LBBLFL}}{Baseline Flag}
    \item{\code{ONTRTFL}}{On or Off Study Treatment Flag}
    \item{\code{ENTMTFL}}{End of Treatment Flag}
    \item{\code{BLTRFL}}{Baseline Result Abnormal, threshold range}
    \item{\code{LBNRIND}}{Refernce Range Indicator}
    \item{\code{LBTRFL}}{Result Abnormal, threshold range}
    \item{\code{LBTRMXFL}}{Most Extreme, based on threshold range}
    \item{\code{LBPRFL}}{Result Abnormal, compared to previous visit}
    \item{\code{LBPRMXFL}}{Most Extreme, comparison to previous visit}
    \item{\code{LBSTRESC}}{Character Result/Finding in Std Format}
    \item{\code{LBSTRESN}}{Numeric Result/Finding in Std Units}
    \item{\code{LBSTRESU}}{Standard Units}
    \item{\code{BLSTRESN}}{Baseline Result}
    \item{\code{BLTMSLO}}{Baseline Times LLN}
    \item{\code{BLTMSHI}}{	Baseline Times ULN}
    \item{\code{CHSTRESN}}{Result Change from Baseline}
    \item{\code{LBTMSLO}}{Result Times Lower Limit of Normal}
    \item{\code{LBTMSHI}}{	Result Times Upper Limit of Normal}
    \item{\code{LBTRVAL}}{Amount Threshold Range}
    \item{\code{PRSTRESN}}{Previous Visit Result}
    \item{\code{LBPRRESN}}{Change from Previous Visit, relative to}
    \item{\code{PRTMSLO}}{Previous Times LLN}
    \item{\code{PRTMSHI}}{Previous Times ULN}
    \item{\code{TRTP}}{ADaM? Description of Planned Arm}
    \item{\code{TRTPCD}}{	ADaM? Planned Arm Code}
    \item{\code{TRTPN}}{ADam Planned Arm Code, Numeric}
    \item{\code{AGE}}{Age in AGEU at RFSTDTC}
    \item{\code{AGEGRP}}{Age Group}
    \item{\code{AGEGRPN}}{Age Group, Numeric }
    \item{\code{RACE}}{Race}
    \item{\code{RACEN}}{Race, Numeric}
    \item{\code{SEX}}{Sex}
    \item{\code{BLLO}}{	Baseline LLN}
    \item{\code{BLHI}}{Baseline ULN}
    \item{\code{LBSTNRLO}}{Reference Range Lower Limit - Std Units}
    \item{\code{LBSTNRHI}}{Reference Range Upper Limit - Std Units}
    \item{\code{PRLO}}{Previous LLN}
    \item{\code{PRHI}}{Previous ULN}
    \item{\code{SAFETY}}{	Safety Population Flag}
    \item{\code{ITT}}{	Intent to Treat Population Flag}
    \item{\code{EFFICACY}}{Efficacy Population Flag}
    \item{\code{COMPLT24}}{Completer of Week 24 Population Flag}
    \item{\code{DSREASAE}}{Discontinued Due to AE?}
    \item{\code{RANDDT}}{Date of Randomization (Visit 3)}
    \item{\code{TRTSTDT}}{Start Date of Treatment}
    \item{\code{LSTDOSDT}}{Date of Last Dose}
    \item{\code{TRTDUR}}{Duration of Treatment}

  }
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(adlbc)
## maybe str(adlbc) ; plot(adlbc) ...
}
\keyword{datasets}
