#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
  # Application title
  titlePanel("Generate table between Adverse Event and Treatment Group"),
  
  # Sidebar with a slider input for number of bins 
  sidebarLayout(
    sidebarPanel(
       #textInput("dataset",
      #             "Data"),
       
       fileInput("dataset", "Data"),
       numericInput("conf", "Confidence Interval", 0.95, min = 0.01, max = 0.99, step = 0.01),
       textInput("preferredTerm", "Preferred Term Column Name"),
       textInput("treatmentGroup", "Treatment Group Column Name"),
       textInput("subjId", "Subject ID Column Name"),
       radioButtons("generateRiskDifference", "Generate Risk Difference ?",
                    c("Yes" = "Yes",
                      "No" = "No")),
       textInput("controlGroup", "Control Group"),
       textInput("sortingGroup", "Sorting Treatment Group"),
       numericInput("minRelativeFreq", "Minimum Relative Frequency", 0.0, min = 0, step = 0.1),
       actionButton("generatetable", "Generate Table")
    ),
    
    # Show a plot of the generated distribution
    mainPanel(
      dataTableOutput("view")
      # textOutput("textOut")
    )
  )
))
