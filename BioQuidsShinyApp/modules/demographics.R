demographicsUI <- function(id) {
  ns <- NS(id)

  tagList(
    titlePanel("Demographics Table"),
    sidebarLayout(
      sidebarPanel(

        fileInput(ns("dataset"), "Data"),
        numericInput(ns("conf"), "Confidence Interval", 0.95, min = 0.01, max = 0.99, step = 0.01),
        selectInput(ns("vars"), "Variables", c(), multiple = TRUE),
        selectInput(ns("labels"), "Labels", c(), multiple = TRUE),
        selectInput(ns("treatmentGroup"), "Treatment Group", c()),
        selectInput(ns("subjId"), "Subject ID", c()),
        actionButton(ns("generatetable"), "Generate Table")
      ),

      # Show a plot of the generated distribution
      mainPanel(
        uiOutput(ns("downloadButtons")),
        dataTableOutput(ns("view"))
      ))
  )
}



demographics <- function(input, output, session) {

  ns <- session$ns

  data <- reactive({
    inFile <- input$dataset

    if (is.null(inFile))
      return(NULL)

    read.csv(inFile$datapath)
  })

  observeEvent(input$dataset, {

    columns <- colnames(data())

    updateSelectInput(session, "vars",
                      choices = columns)

    updateSelectInput(session, "labels",
                      choices = columns)

    updateSelectInput(session, "treatmentGroup",
                      choices = columns)

    updateSelectInput(session, "subjId",
                      choices = columns)

  })


  observeEvent(input$generatetable, {
    output$view <- renderDataTable({

      phTLGs::demog(data = data(),
                    vars = input$vars,
                    labels = input$labels,
                    treatmentGroup = input$treatmentGroup,
                    subjId = input$subjId
      )

    },
    filter = 'top',
    rownames = FALSE
    )

    output$downloadButtons <- renderUI({
      tagList(
        downloadButton(ns('wordOutput'), 'Export to Word', 'export-to-word'),
        downloadButton(ns('pdfOutput'), 'Export to PDF', 'export-to-pdf'),
        downloadButton(ns('excelOutput'), 'Export to Excel', 'export-to-excel')
      )
    })

    wordFilePath <- phTLGs::demog(data = data(),
                                  vars = input$vars,
                                  labels = input$labels,
                                  treatmentGroup = input$treatmentGroup,
                                  subjId = input$subjId,
                                  print2 = "doc",
                                  filePath = paste(getwd(), "/demographics.docx", sep="")
    )

    output$wordOutput <- downloadHandler(
      filename = function() {
        paste("demographics-", Sys.Date(), ".docx", sep="")
      },
      content = function(file) {
        file.copy(wordFilePath, file)
      }
    )


    pdfFilePath <- phTLGs::demog(data = data(),
                                 vars = input$vars,
                                 labels = input$labels,
                                 treatmentGroup = input$treatmentGroup,
                                 subjId = input$subjId,
                                 print2 = "pdf",
                                 filePath = paste(getwd(), "/demographics.pdf", sep="")
    )

    output$pdfOutput <- downloadHandler(
      filename = function() {
        paste("demographics-", Sys.Date(), ".pdf", sep="")
      },
      content = function(file) {
        file.copy(pdfFilePath, file)
      }
    )


    excelFilePath <- phTLGs::demog(data = data(),
                                   vars = input$vars,
                                   labels = input$labels,
                                   treatmentGroup = input$treatmentGroup,
                                   subjId = input$subjId,
                                   print2 = "excel",
                                   filePath = paste(getwd(), "/demographics.xlsx", sep="")
    )

    output$excelOutput <- downloadHandler(
      filename = function() {
        paste("demographics-", Sys.Date(), ".xlsx", sep="")
      },
      content = function(file) {
        file.copy(excelFilePath, file)
      }
    )
  })

}
