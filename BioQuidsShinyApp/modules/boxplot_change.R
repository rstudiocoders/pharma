boxpltchangeUI <- function(id) {
  ns <- NS(id)

  tagList(
    titlePanel("Boxplot Change Over Time"),
    sidebarLayout(
      sidebarPanel(

        fileInput(ns("data"), "Data"),
        selectInput(ns("base"), "Baseline Value", c()),
        selectInput(ns("param_col"), "Parameter", c()),
        selectInput(ns("param_val"), "Parameter Value", c()),
        selectInput(ns("chg_col"), "Measurement", c()),
        selectInput(ns("treatment"), "Treatment", c()),
        selectInput(ns("visit_col"), "Visit", c()),
        selectInput(ns("opt_atpt"), "Analysis Time Point", c()),
        selectInput(ns("opt_atpt_val"), "Analysis Time Point Value", c()),
        selectInput(ns("x_axis_test_val"), "Visits on x-axis", c(),multiple = TRUE),
        selectInput(ns("subjid"), "Subject IDs", c()),
        #textInput(ns("page_no"), "Page Numbers", c()),

        actionButton(ns("generateplot"), "Generate Plot"),
        width = 3),

      # Show a plot of the generated distribution
      mainPanel(
        plotOutput(ns("view"),height = 650, width = 1000)
      ))
  )
}



boxpltchange <- function(input, output, session) {

  observeEvent(input$data, {
    inFile <- input$data

    if (is.null(inFile))
      return(NULL)

    data <- read.csv(inFile$datapath)

    columns <- colnames(data)


    updateSelectInput(session, "base",
                      choices = columns,
                      selected = NULL)

    updateSelectInput(session, "param_col",
                      choices = columns,
                      selected = NULL)

    updateSelectInput(session, "chg_col",
                      choices = columns,
                      selected = NULL)

    updateSelectInput(session, "treatment",
                      choices = columns,
                      selected = NULL)

    updateSelectInput(session, "visit_col",
                      choices = columns,
                      selected = NULL)

    updateSelectInput(session, "opt_atpt",
                      choices = columns,
                      selected = NULL)

    updateSelectInput(session, "baseline_results",
                      choices = columns,
                      selected = NULL)

    updateSelectInput(session, "subjid",
                      choices = columns,
                      selected = NULL)

  })

  observeEvent(input$param_col, {
    inFile <- input$data

    if (is.null(inFile))
      return(NULL)

    data <- read.csv(inFile$datapath)

    uniqueParam <- unique(data[input$param_col])
    updateSelectInput(session, "param_val",
                      choices = uniqueParam)


  })

  observeEvent(input$visit_col, {
    inFile <- input$data

    if (is.null(inFile))
      return(NULL)

    data <- read.csv(inFile$datapath)

    uniqueVisits <- unique(data[input$visit_col])
    updateSelectInput(session, "x_axis_test_val",
                      choices = uniqueVisits )
  })


  observeEvent(input$opt_atpt, {
    inFile <- input$data

    if (is.null(inFile))
      return(NULL)

    data <- read.csv(inFile$datapath)

    uniqueOpt <- unique(data[input$opt_atpt])
    updateSelectInput(session, "opt_atpt_val",
                      choices = uniqueOpt)

  })


  observeEvent(input$generateplot, {
    output$view <- renderPlot({

      inFile <- input$data

      if (is.null(inFile))
        return(NULL)

      data <- read.csv(inFile$datapath)


      phTLGs::boxplot_change(data = data,
                             base = input$base,
                             param_col = input$param_col,
                             param_val = input$param_val,
                             chg_col = input$chg_col,
                             treatment = input$treatment,
                             visit_col = input$visit_col,
                             opt_atpt = input$opt_atpt,
                             opt_atpt_val = input$opt_atpt_val,
                             x_axis_test_val = input$x_axis_test_val,
                             subjid = input$subjid
      )

    })
  })

}
