medicalHistoryUI <- function(id) {
  ns <- NS(id)

  tagList(
    titlePanel("Medical History Table"),
    sidebarLayout(
      sidebarPanel(

        fileInput(ns("mhDataset"), "Medical History DataSet"),
        fileInput(ns("patientsDataset"), "Patients DataSet"),
        selectInput(ns("preferredTerm"), "Preferred Term", c()),
        selectInput(ns("treatmentGroup"), "Treatment Group", c()),
        selectInput(ns("subjId"), "Subject ID", c()),
        selectInput(ns("systemOrganClass"), "System Organ Class", c()),
        actionButton(ns("generateTable"), "Generate Table")
      ),

      # Show a plot of the generated distribution
      mainPanel(
        uiOutput(ns("downloadButtons")),
        dataTableOutput(ns("view"))
      ))
  )
}


medicalHistory <- function(input, output, session) {

  ns <- session$ns

  patientsData <- reactive({
    inFile <- input$patientsDataset

    if (is.null(inFile))
      return(NULL)

    read.csv(inFile$datapath)
  })

  mhData <- reactive({
    inFile <- input$mhDataset

    if (is.null(inFile))
      return(NULL)

    read.csv(inFile$datapath)
  })

  observeEvent(input$mhDataset, {

    columns <- colnames(mhData())

    updateSelectInput(session, "preferredTerm",
                      choices = columns)

    updateSelectInput(session, "subjId",
                      choices = columns)

    updateSelectInput(session, "systemOrganClass",
                      choices = columns)
  })


  observeEvent(input$patientsDataset, {

    columns <- colnames(patientsData())

    updateSelectInput(session, "treatmentGroup",
                      choices = columns)

  })

  observeEvent(input$generateTable, {

    output$view <- renderDataTable({

      phTLGs::med_hx(data = mhData(),
                     patientsData = patientsData(),
                     preferredTerm = input$preferredTerm,
                     treatmentGroup = input$treatmentGroup,
                     subjId = input$subjId,
                     systemOrganClass = input$systemOrganClass
      )

    },
    filter = 'top',
    rownames = FALSE
    )

    output$downloadButtons <- renderUI({
      tagList(
        downloadButton(ns('wordOutput'), 'Export to Word', 'export-to-word'),
        downloadButton(ns('pdfOutput'), 'Export to PDF', 'export-to-pdf'),
        downloadButton(ns('excelOutput'), 'Export to Excel', 'export-to-excel')
      )
    })

    wordFilePath <- phTLGs::med_hx(data = mhData(),
                                   patientsData = patientsData(),
                                   preferredTerm = input$preferredTerm,
                                   treatmentGroup = input$treatmentGroup,
                                   subjId = input$subjId,
                                   systemOrganClass = input$systemOrganClass,
                                   print2 = "doc",
                                   filePath = paste(getwd(), "/medicalhistory.docx", sep="")
    )

    output$wordOutput <- downloadHandler(
      filename = function() {
        paste("medicalhistory-", Sys.Date(), ".docx", sep="")
      },
      content = function(file) {
        file.copy(wordFilePath, file)
      }
    )


    pdfFilePath <- phTLGs::med_hx(data = mhData(),
                                  patientsData = patientsData(),
                                  preferredTerm = input$preferredTerm,
                                  treatmentGroup = input$treatmentGroup,
                                  subjId = input$subjId,
                                  systemOrganClass = input$systemOrganClass,
                                  print2 = "pdf",
                                  filePath = paste(getwd(), "/medicalhistory.pdf", sep="")
    )

    output$pdfOutput <- downloadHandler(
      filename = function() {
        paste("medicalhistory-", Sys.Date(), ".pdf", sep="")
      },
      content = function(file) {
        file.copy(pdfFilePath, file)
      }
    )


    excelFilePath <- phTLGs::med_hx(data = mhData(),
                                    patientsData = patientsData(),
                                    preferredTerm = input$preferredTerm,
                                    treatmentGroup = input$treatmentGroup,
                                    subjId = input$subjId,
                                    systemOrganClass = input$systemOrganClass,
                                    print2 = "excel",
                                    filePath = paste(getwd(), "/medicalhistory.xlsx", sep="")
    )

    output$excelOutput <- downloadHandler(
      filename = function() {
        paste("medicalhistory-", Sys.Date(), ".xlsx", sep="")
      },
      content = function(file) {
        file.copy(excelFilePath, file)
      }
    )
  })
}
